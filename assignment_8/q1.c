#include <stdio.h>
struct student {
    char name[20];
    char subject[15];
    int marks;
};
int main()
{
  int students;
  printf("enter the number of students : ");
  scanf("%d",&students);
  struct student stu[students];
  for(int i =0;i<students;i++){
    printf("enter the first name of student number %d :",i+1);
    scanf("%s",&stu[i].name);
    printf("enter the subject of the student number %d : ",i+1);
    scanf("%s",&stu[i].subject);
    printf("enter the marks for the %s subject of student %d :",stu[i].subject,i+1);
    scanf("%d",&stu[i].marks);
  }
  for(int i=0;i<students;i++){
    printf("\nstudent number %d : \n first name : %s \n subject : %s \n marks : %d \n",i+1,stu[i].name,stu[i].subject,stu[i].marks);
  }
}
