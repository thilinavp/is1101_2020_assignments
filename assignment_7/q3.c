#include <stdio.h>
#include <string.h>
void printSum(int a[3][3],int b[3][3]){
    printf("sum of matrix a and b : \n");
    for(int i =0; i<3;i++){
        for(int j=0;j<3;j++){
            printf("%d \t",a[i][j]+b[i][j]);
        }
         printf("\n");
    }
    printf("\n");
}
void printMul(int a[3][3],int b[3][3]){
    int temp= 0;
    int x[3][3];
    for(int k =0; k<3;k++){
        for(int l=0;l<3;l++){
            for(int i=0;i<3;i++){
                temp += a[k][i]*b[i][l];
            }
            x[k][l]=temp;
            temp =0;
        }
    }
    printf("product of matrix a and b : \n \n");
    for(int i =0; i<3;i++){
        for(int j=0;j<3;j++){
            printf("%d \t",x[i][j]);
        }
         printf("\n");
    }
    printf("\n");
}

int main()
{
    int a[3][3];
    int b[3][3];
    printf(" enter values for matrix a (3 * 3 matrix ): \n");
    for(int i =0; i<3;i++){
        for(int j=0;j<3;j++){
            scanf("%d",&a[i][j]);
        }
    }
    printf(" enter values for matrix b (3 * 3 matrix) : \n");
    for(int i =0; i<3;i++){
        for(int j=0;j<3;j++){
            scanf("%d",&b[i][j]);
        }
    }
    printSum(a,b);
    printMul(a,b);
}
